#include "mainwindow.hh"
#include "ui_mainwindow.h"
#include "launcher.hh"
#include "settings.hh"

#include <QList>
#include <QListWidget>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    m_settings.reset( new Settings );
    m_launcher.reset( new Launcher(m_settings) );

    ui->setupUi(this);

    showModList();

    // Refresh mod list according to the file path
    QObject::connect(m_settings.get(), &QDialog::accepted, this, &MainWindow::showModList);

    // Save selected mods to settings
    QObject::connect(ui->listWidget, &QListWidget::itemChanged, m_settings.get(), &Settings::saveCheckedMod);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionPaths_triggered()
{
    m_launcher->showOptions();
}

void MainWindow::on_pushButton_clicked()
{
    m_launcher->launch();
}

void MainWindow::showModList()
{
    ui->listWidget->clear();
    QVector< QString >mods =  m_launcher->loadMoadList();

    // Add mods to the list
    for(int i = 0; i < mods.size(); ++i)
    {
        QListWidgetItem* item = new QListWidgetItem(mods.at(i));
        item->setCheckState(Qt::Unchecked);
        ui->listWidget->addItem(item);
    }

    // Previously checked mods
    checkMods();
}

void MainWindow::checkMods()
{
    QStringList mods = m_launcher->getCheckedMods();

    foreach(QString str, mods)
    {
        QList< QListWidgetItem *> item = ui->listWidget->findItems(str, Qt::MatchExactly);

        if(item.count() >= 1)
        {
            item.at(0)->setCheckState(Qt::Checked);
        }
    }
}
