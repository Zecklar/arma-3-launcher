#include "launcher.hh"
#include "settings.hh"

#include <QDir>
#include <QDirIterator>
#include <QProcess>

Launcher::Launcher(std::shared_ptr<Settings> p_settings)
{
    settings = p_settings;
}

Launcher::~Launcher()
{

}

void Launcher::showOptions()
{
    settings->exec();
}

void Launcher::launch()
{
    QProcess* process = new QProcess();
    QString path = settings->getPath("arma3");
    path.replace(QString("\\"), QString("\\\\"));
    process->start(path, QStringList() << getArguments());
}

QStringList Launcher::getCheckedMods()
{
    return settings->getCheckedMods();
}

QString Launcher::getArguments()
{
    QString mods = settings->getCheckedMods().join(";");

    if(mods.isEmpty())
    {
        return "";
    }
    else
    {
        return "-mod=" + mods;
    }
}

QVector< QString > Launcher::loadMoadList()
{
    QString path = settings->getPath("modfolder");
    path.replace(QString("\\"), QString("\\\\"));
    QDir dir(path);

    // Set directory name filter
    QStringList filters;
    filters << "@*";
    dir.setNameFilters(filters);
    QDirIterator it(dir);

    QVector< QString >tmp(0);

    while(it.hasNext())
    {
        if(!it.fileName().isEmpty())
        {
            tmp.push_back(it.fileName());
        }

        it.next();
    }

    return tmp;
}
