#include "settings.hh"
#include "ui_settings.h"
#include <QTextStream>
#include <QFile>
#include <QFileDialog>
#include <QDebug>

Settings::Settings(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Settings),
    filename("config.xml")
{
    ui->setupUi(this);

    config = readContent();

    // Set the paths to option window on start
    setPaths();
}

Settings::~Settings()
{
    delete ui;
}

void Settings::on_buttonBox_accepted()
{
    QString armapath = ui->arma3path->text();

    // Arma3 tag
    // Gotta use elementsByTagName because there are no implementation
    // for elementsByID
    QDomNodeList armanode = config.elementsByTagName("arma3");

    // New path to Arma3 and mod folder
    QDomText armatext = config.createTextNode(armapath);

    // Get the old value and replace it with the new one
    QDomElement armaelement = armanode.at(0).toElement();
    armaelement.replaceChild(armatext, armaelement.firstChild());

    // ---

    // Change mod filepath
    QString modpath = ui->modpath->text();
    replaceNodeString(modpath, "modfolder");
}

QDomDocument Settings::readContent()
{
    QFile file(filename);

    if(!file.open(QIODevice::ReadOnly))
    {
        qDebug() << "Error while opening file: " << file.errorString() << endl;
    }

    QDomDocument doc;
    QString error = "";
    if(!doc.setContent(&file, &error))
    {
        qDebug() << "Error while setting content: " << error << endl;
        file.close();
    }

    return doc;
}

void Settings::setPaths()
{
    ui->arma3path->setText(getPath("arma3"));
    ui->modpath->setText(getPath("modfolder"));
}

void Settings::replaceNodeString(QString nodeString, QString node)
{
    QDomNodeList modNode = config.elementsByTagName(node);

    QDomText text = config.createTextNode(nodeString);

    QDomElement modelement = modNode.at(0).toElement();

    // If there is no mods
    if(modelement.firstChild().isNull())
    {
        modelement.appendChild(text);
    }
    else
    {
        // Replace the modstring in "mods" node
        modelement.replaceChild( text, modelement.firstChild() );
    }

    writeToConfig();
}

QString Settings::getPath(QString node)
{
    QDomElement e = config.elementsByTagName(node).at(0).toElement();
    QString path = e.text();

    return path;
}

QStringList Settings::getCheckedMods()
{
    QDomNodeList modNode = config.elementsByTagName("mods");
    QString modString = modNode.at(0).toElement().text();

    // Split to separate nodes
    QStringList mods = modString.split(";");

    return mods;
}

void Settings::saveCheckedMod(QListWidgetItem* mod)
{
    QString name = mod->text();
    QStringList mods = getCheckedMods();

    if(mod->checkState() != Qt::Checked && mods.contains(name))
    {
        int index = mods.indexOf(name);
        mods.removeAt(index);
    }
    else if(mod->checkState() == Qt::Checked && !mods.contains(name))
    {
        mods << name;
    }

    // If mod is checked and it's in stringlist, nothing happens


    QString modString = "";
    modString = mods.join(";");

    replaceNodeString(modString, "mods");
}

void Settings::writeToConfig()
{
    QFile file(filename);

    if(!file.open(QIODevice::WriteOnly))
    {
        qDebug() << "Error while opening file: " << file.errorString() << endl;
    }

    // Save the change
    file.resize(0);
    QTextStream stream;
    stream.setDevice(&file);
    config.save(stream, 2);
    file.close();
}

void Settings::on_arma3browse_clicked()
{
    QString path = QFileDialog::getOpenFileName(this, tr("Browse file"), ui->arma3path->text());

    replaceNodeString(path, "arma3");
    setPaths();
}

void Settings::on_modbrowse_clicked()
{
    QString path = QFileDialog::getExistingDirectory(this, tr("Browse Directory"),
                   ui->modpath->text(), QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);

    replaceNodeString(path, "modfolder");
    setPaths();
}
