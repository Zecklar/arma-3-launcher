#ifndef MAINWINDOW_HH
#define MAINWINDOW_HH

#include <QMainWindow>
#include <memory>

class Launcher;
class Settings;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    // Starts the settings window
    void on_actionPaths_triggered();
    // Launch button
    void on_pushButton_clicked();

private:
    Ui::MainWindow *ui;

    std::unique_ptr< Launcher > m_launcher;
    std::shared_ptr< Settings > m_settings;

    void checkMods();

    // If the modfolder path is changed
    void showModList();
};

#endif // MAINWINDOW_HH
