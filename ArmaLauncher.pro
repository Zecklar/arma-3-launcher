#-------------------------------------------------
#
# Project created by QtCreator 2015-01-27T11:45:31
#
#-------------------------------------------------

QT       += core gui xml

CONFIG += c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ArmaLauncher
TEMPLATE = app


SOURCES += main.cc\
        mainwindow.cc \
    launcher.cc \
    settings.cc

HEADERS  += mainwindow.hh \
    launcher.hh \
    settings.hh

FORMS    += mainwindow.ui \
    settings.ui

RESOURCES +=
