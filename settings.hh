#ifndef SETTINGS_HH
#define SETTINGS_HH

#include <QDialog>
#include <QtXml/QDomDocument>
#include <QStringList>
#include <QString>
#include <QListWidgetItem>

namespace Ui {
class Settings;
}

class Settings : public QDialog
{
    Q_OBJECT

public:
    explicit Settings(QWidget *parent = 0);
    ~Settings();

    // Parameter = node name
    QString getPath(QString node);

    QStringList getCheckedMods();

public slots:
    void saveCheckedMod(QListWidgetItem* mod);

private slots:
    void on_buttonBox_accepted();

    void on_arma3browse_clicked();

    void on_modbrowse_clicked();

private:
    Ui::Settings *ui;
    QString filename;
    QDomDocument config;

    void writeToConfig();
    QDomDocument readContent();
    void setPaths();
    void replaceNodeString(QString nodeString, QString node);
};

#endif // SETTINGS_HH
