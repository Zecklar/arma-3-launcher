#ifndef LAUNCHER_HH
#define LAUNCHER_HH

#include <QVector>
#include <QString>
#include <memory>
#include <QObject>
#include <QListWidgetItem>
#include <QStringList>


class Settings;

class Launcher: public QObject
{
    Q_OBJECT
public:

    Launcher(std::shared_ptr< Settings > p_settings);
    ~Launcher();

    void showOptions();
    void launch();

    QStringList getCheckedMods();
    QVector< QString > loadMoadList();

private:
    std::shared_ptr< Settings > settings;

    QString getArguments();
};

#endif // LAUNCHER_HH
